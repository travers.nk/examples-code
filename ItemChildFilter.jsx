import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import {toJsonString} from '../../../../../imports/helpers/stringHelpers.js'

export default class ItemChildFilter extends Component {

    constructor (props) {
        super(props);

        this.state = {
            isChecked: false,
            isCheckedOut: this.props.childCheck,
            isClearOut: false,
        };

        this.clickCheck = this.clickCheck.bind(this);
    };

    componentWillMount() {
        const param = this.props.param;
        this.setState({
            isClearOut: this.props.isClearAll,
        });
        if (checkedArray.get()['item'].indexOf(toJsonString(param, this.props.child.path)) > -1){
            this.setState({
                isChecked: true
            });
        }
    }


    componentDidUpdate(previousProps, previousState) {

        if(previousState.isCheckedOut !== this.props.childCheck){
            this.setState({
                isChecked : this.props.childCheck,
                isCheckedOut : this.props.childCheck
            });
        }

        if (previousState.isClearOut !== this.props.isClearAll) {
            this.setState({
                isChecked: false,
                isClearOut: this.props.isClearAll,
            });
        }
    }

    clickCheck(e){
        e.preventDefault();
        if (this.state.isChecked) {
            this.setState({
                isChecked: false
            });
            this.removeCheckedArray(this.props.child);
        } else {
            this.setState({
                isChecked: true,
            });
            this.addCheckedArray(this.props.child);
        }

    }


    removeCheckedArray(item) {
        console.log('remove child');
        let position = checkedArray.get()['item'].indexOf(toJsonString(param, item.path));
        if (position >= 0) {
            checkedArray.get()['item'].splice(position, 1);
        }
    }

    addCheckedArray(item){
        console.log('add child');
        const param = this.props.param;
        if (checkedArray.get()['item'].indexOf(toJsonString(param, item.path)) == -1) {
            checkedArray.get()['item'].push(toJsonString(param, item.path));
        }
    }

    render() {

        return (
            <ul className="subgamma-list">
                <li>
                    <input id={this.props.child._id} type="checkbox" value="1" checked={this.state.isChecked}/>
                    <label htmlFor={this.props.child._id} onClick={this.clickCheck}>{this.props.child.path}</label>
                </li>
            </ul>
        )
    }
}