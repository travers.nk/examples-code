import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import {Categories} from '../../../../../lib/collections/categories.js';
import {toJsonString} from '../../../../../imports/helpers/stringHelpers.js';



export default class LayoutFilter extends Component {
    constructor (props) {
        super(props);
        this.state = {
            isCheckedAll: false,
            isClearAll: false,
            checkedMenuItem: '',
        };
        this.clickBack = this.clickBack.bind(this);
        this.chooseAll = this.chooseAll.bind(this);
        this.clearAll = this.clearAll.bind(this);
        this.clickApply = this.clickApply.bind(this);

    };

    setCheckedMenuItem(item) {
        this.setState({ checkedMenuItem: item });
    }

    chooseAll(e) {
        e.preventDefault();
        const inputs = document.querySelectorAll('.product-choose input');
        const inputsArray = Object.keys(inputs).map(key => inputs[key].id);
        const inputsArrayMongoId = inputsArray.map(i => new Meteor.Collection.ObjectID(i));
        const cats = Categories.find({_id: {$in: inputsArrayMongoId}}).fetch();
        const param = this.state.checkedMenuItem;
        for (let i = 0; i< cats.length; i++){
            if (checkedArray.get()['item'].indexOf(toJsonString(param, cats[i].path)) == -1){
                checkedArray.get()['item'].push(toJsonString(param, cats[i].path)); //TODO check if exist
            }
        }
        this.setState({isCheckedAll: true});
    }

    clearAll(e) {
        e.preventDefault();

        const inputs = document.querySelectorAll('.product-choose input');
        const inputsArray = Object.keys(inputs).map(key => inputs[key].id);
        const inputsArrayMongoId = inputsArray.map(i => new Meteor.Collection.ObjectID(i));
        const cats = Categories.find({_id: {$in: inputsArrayMongoId}}).fetch();
        const param = this.state.checkedMenuItem;

        cats.map((cat) => (
            checkedArray.get()['item'].splice(checkedArray.get()['item'].indexOf(toJsonString(param, cat.path)), 1)
        ));
        this.setState({isClearAll: !this.state.isClearAll});
        this.setState({isCheckedAll: false});
    }

    clickBack(e){
        e.preventDefault();
        checkedArray.set({item:[]});
        this.props.history.goBack();
    }

    clickApply(e){
        const id1 = this.props.match.params.id1;
        const id2 = this.props.match.params.id2;
        const url = '/list-product'+'/'+id1+'/'+id2;
        this.props.history.push(url);
    }

    renderHeader() {
        const categoryName = this.props.match.params.id2.replace('-', '/');
        return (
            <div>
                <header className="page-of-filter">
                    <a className="icon-back" onClick={this.clickBack}></a>
                    <h2 className="name-of-category">{categoryName}</h2>
                </header>
                <button className="Choose-all" onClick={this.chooseAll}><i>tout sélectionner</i></button>
            </div>
        )
    }

    renderFooter(){
            return(
                <div className="bot-but">
                    <button className="cancel" onClick={this.clearAll}><i>effacer</i></button>
                    <button className="apply" onClick={this.clickApply}><i>appliquer</i></button>
                </div>
            )
    }

    render() {
        const Inner = this.props.inner;
        return (
            <div className="page-of-filter">
                {this.renderHeader()}
                <Inner {...this.props} isCheckedAll={this.state.isCheckedAll} isClearAll={this.state.isClearAll}
                       getCheckedMenuItem={ item => this.setCheckedMenuItem(item) }/>
                {this.renderFooter()}
            </div>
        )
    }
}